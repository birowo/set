package set

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func equal(t *testing.T) func(Set[int], Set[int]) bool {
	t.Helper()

	return func(s1, s2 Set[int]) bool {
		return s1.Equal(s2)
	}
}

func TestSetDelete(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		v    []int
		want Set[int]
	}{
		{
			name: "delete int",
			s:    New(1, 2, 3),
			v:    []int{1, 2},
			want: New(3),
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			tt.s.Delete(tt.v...)

			if diff := cmp.Diff(tt.want, tt.s, cmp.Comparer(equal(t))); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetDifference(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		t    Set[int]
		want Set[int]
	}{
		{
			name: "difference int 1",
			s:    New(1, 2, 3),
			t:    New(1, 2, 4, 5),
			want: New(3),
		},
		{
			name: "difference int 2",
			s:    New(1, 2, 4, 5),
			t:    New(1, 2, 3),
			want: New(4, 5),
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if diff := cmp.Diff(tt.want, tt.s.Difference(tt.t), cmp.Comparer(equal(t))); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetEqual(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		t    Set[int]
		want bool
	}{
		{
			name: "equal",
			s:    New(1, 2),
			t:    New(2, 1),
			want: true,
		},
		{
			name: "not equal",
			s:    New(1, 2),
			t:    New(1, 3),
			want: false,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if diff := cmp.Diff(tt.want, tt.s.Equal(tt.t)); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetContains(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		v    int
		want bool
	}{
		{
			name: "contains",
			s:    New(1),
			v:    1,
			want: true,
		},
		{
			name: "not contains int",
			s:    New(1),
			v:    2,
			want: false,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if diff := cmp.Diff(tt.want, tt.s.Contains(tt.v)); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetContainsAll(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		v    []int
		want bool
	}{
		{
			name: "contains",
			s:    New(1, 2, 3),
			v:    []int{1, 2},
			want: true,
		},
		{
			name: "not contains all",
			s:    New(1, 2),
			v:    []int{1, 2, 3},
			want: false,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if diff := cmp.Diff(tt.want, tt.s.ContainsAll(tt.v...)); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetContainsAny(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		v    []int
		want bool
	}{
		{
			name: "contains any",
			s:    New(1, 2),
			v:    []int{1, 3},
			want: true,
		},
		{
			name: "not contains any",
			s:    New(1, 2),
			v:    []int{3},
			want: false,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if diff := cmp.Diff(tt.want, tt.s.ContainsAny(tt.v...)); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetInsert(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		v    int
		want Set[int]
	}{
		{
			name: "insert int",
			s:    New(1),
			v:    2,
			want: New(1, 2),
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			tt.s.Insert(tt.v)

			if diff := cmp.Diff(tt.want, tt.s); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetIntersection(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		t    Set[int]
		want Set[int]
	}{
		{
			name: "intersection int with same len",
			s:    New(1, 2, 3),
			t:    New(2, 3, 5),
			want: New(2, 3),
		},
		{
			name: "intersection int with different len",
			s:    New(1, 2, 3, 4),
			t:    New(2, 3, 5),
			want: New(2, 3),
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if diff := cmp.Diff(tt.want, tt.s.Intersection(tt.t), cmp.Comparer(equal(t))); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetIsSuperset(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		t    Set[int]
		want bool
	}{
		{
			name: "is superset",
			s:    New(1, 2, 3),
			t:    New(1, 2),
			want: true,
		},
		{
			name: "is not superset",
			s:    New(1, 2),
			t:    New(1, 2, 3),
			want: false,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if diff := cmp.Diff(tt.want, tt.s.IsSuperset(tt.t)); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetLen(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		want int
	}{
		{
			name: "len",
			s:    New(1, 2),
			want: 2,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if diff := cmp.Diff(tt.want, tt.s.Len()); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetPopAny(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		s        Set[int]
		want     int
		wantBool bool
		wantSet  Set[int]
	}{
		{
			name:     "pop",
			s:        New(1),
			want:     1,
			wantBool: true,
			wantSet:  New[int](),
		},
		{
			name:     "no pop",
			s:        New[int](),
			want:     0,
			wantBool: false,
			wantSet:  New[int](),
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			got, ok := tt.s.PopAny()

			if diff := cmp.Diff(tt.want, got); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
			if diff := cmp.Diff(tt.wantBool, ok); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
			if diff := cmp.Diff(tt.wantSet, tt.s); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetString(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		want string
	}{
		{
			name: "string of int",
			s:    New(1),
			want: "[1]",
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if diff := cmp.Diff(tt.want, tt.s.String()); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetValues(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		want []int
	}{
		{
			name: "valid int set",
			s:    New(1, 2),
			want: []int{1, 2},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if diff := cmp.Diff(tt.want, tt.s.Values(), cmpopts.SortSlices(func(i, j int) bool {
				return i < j
			})); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}

func TestSetUnion(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		s    Set[int]
		t    Set[int]
		want Set[int]
	}{
		{
			name: "union",
			s:    New(1, 2),
			t:    New(2, 3),
			want: New(1, 2, 3),
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if diff := cmp.Diff(tt.want, tt.s.Union(tt.t), cmp.Comparer(equal(t))); diff != "" {
				t.Errorf("(-want +got):\n%s", diff)
			}
		})
	}
}
