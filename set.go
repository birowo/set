package set

import (
	"fmt"
)

// Set is a set of comparables.
type Set[V comparable] map[V]struct{}

// New returns a Set from the given values.
func New[V comparable](vs ...V) Set[V] {
	s := make(Set[V], len(vs))
	for _, v := range vs {
		s[v] = struct{}{}
	}
	return s
}

// Clone returns a new Set that a copy of `s`.
func (s Set[V]) Clone() Set[V] {
	t := make(Set[V], len(s))
	for v, _ := range s {
		t[v] = struct{}{}
	}
	return t
}

// Delete removes the given values from `s`.
func (s Set[V]) Delete(v ...V) {
	for _, x := range v {
		delete(s, x)
	}
}

// Difference returns a Set whose values are in `s` and not in `t`.
//
// For example:
//
//	s = {a1, a2, a3}
//	t = {a1, a2, a4, a5}
//	s.Difference(t) = {a3}
//	t.Difference(s) = {a4, a5}
func (s Set[V]) Difference(t Set[V]) Set[V] {
	u := make(Set[V], len(s))
	for k, _ := range s {
		if _, ok := t[k]; !ok {
			u[k] = struct{}{}
		}
	}
	return u
}

// Intersection returns a new Set whose values are included in both `s` and `t`.
//
// For example:
//
//	s = {a1, a2}
//	t = {a2, a3}
//	s.Intersection(t) = {a2}
func (s Set[V]) Intersection(t Set[V]) Set[V] {
	var u, walk, other Set[V]
	if len(s) < len(t) {
		walk = s
		other = t
		u = make(Set[V], len(s))
	} else {
		walk = t
		other = s
		u = make(Set[V], len(t))
	}
	for k, _ := range walk {
		if _, ok := other[k]; ok {
			u[k] = struct{}{}
		}
	}
	return u
}

// Equal returns true iff `s` is equal to `t`.
//
// Two sets are equal if their underlying values are identical not considering
// order.
func (s Set[V]) Equal(t Set[V]) bool {
	if len(s) == len(t) {
		for k, _ := range t {
			if _, ok := s[k]; !ok {
				return false
			}
		}
	} else {
		return false
	}
	return true
}

// Contains returns true iff `s` contains a given value.
func (s Set[V]) Contains(v V) bool {
	_, ok := s[v]
	return ok
}

// ContainsAll returns true iff `s` contains all the given values.
func (s Set[V]) ContainsAll(v ...V) bool {
	for _, x := range v {
		if _, ok := s[x]; !ok {
			return false
		}
	}
	return true
}

// ContainsAny returns true iff `s` contains any of the given values.
func (s Set[V]) ContainsAny(v ...V) bool {
	for _, x := range v {
		if _, ok := s[x]; ok {
			return true
		}
	}
	return false
}

// Insert adds the given values to `s`.
func (s Set[V]) Insert(v ...V) {
	for _, x := range v {
		s[x] = struct{}{}
	}
}

// IsSuperset returns true iff `t` is a superset of `s`.
func (s Set[V]) IsSuperset(t Set[V]) bool {
	for k, _ := range t {
		if _, ok := s[k]; !ok {
			return false
		}
	}
	return true
}

// Len returns the size of `s`.
func (s Set[V]) Len() int {
	return len(s)
}

// PopAny returns a single value randomly chosen and removes it from `s`.
func (s Set[V]) PopAny() (v V, _ bool) {
	for k, _ := range s {
		delete(s, k)
		return k, true
	}
	return v, false
}

// String implements fmt.Stringer.
func (s Set[V]) String() string {
	return fmt.Sprint(s.Values())
}

// Values returns the underlying values of `s` as a slice.
func (s Set[V]) Values() []V {
	v := make([]V, 0, len(s))
	for k, _ := range s {
		v = append(v, k)
	}
	return v
}

// Union returns a new Set whose values are included in either `s` or `t`.
//
// For example:
//
//	s = {a1, a2}
//	t = {a3, a4}
//	s.Union(t) = {a1, a2, a3, a4}
//	t.Union(s) = {a1, a2, a3, a4}
func (s Set[V]) Union(t Set[V]) Set[V] {
	u := make(Set[V], len(s)+len(t))
	for k, _ := range s {
		u[k] = struct{}{}
	}
	for k, _ := range t {
		u[k] = struct{}{}
	}
	return u
}
